﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    private Image healthBar;

    public GameObject player;
    private HeroineClass playerClass;

    private void Awake()
    {
        healthBar = GetComponent<Image>();
        playerClass = player.GetComponent<PlayerStats>().player;
    }

    private void LateUpdate()
    {
        // Gets reference of the player
        playerClass = player.GetComponent<PlayerStats>().player;

        // Sets the fill amount of the image component equal to the quotient of current hp and max hp
        healthBar.fillAmount = (float)playerClass.CurrentHP / (float)playerClass.Stats.MaxHP;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class EnemyStats : MonoBehaviour
{
    public MonsterClass monster;
    private NormalAttack normalAttack;

    public int expBaseVal;

    private void Awake()
    {
        monster = new MonsterClass();
        normalAttack = new NormalAttack();
    }

    // Use this for initialization
    void Start()
    {
        // Initializes Monster
        monster.Initialize();

        // Adds monster's skills
        monster.Skill.Add(normalAttack);

        // Sets monster's exp value based on their level
        monster.EXPval = expBaseVal * monster.LVL;
    }

    // Update is called once per frame
    void Update()
    {
        if (!monster.Alive) Die();
    }

    // Animation Event
    void DamageTo()
    {
        MonsterClass enemyMonster = this.gameObject.GetComponent<EnemyStats>().monster;
        HeroineClass playerHeroine = this.gameObject.GetComponent<AI>().player.gameObject.GetComponent<PlayerStats>().player;

        enemyMonster.Skill[0].activateSkill(enemyMonster, playerHeroine);
    }

    // Destroys gameobject when it dies
    public void Die()
    {
        Destroy(this.gameObject);
    }
}

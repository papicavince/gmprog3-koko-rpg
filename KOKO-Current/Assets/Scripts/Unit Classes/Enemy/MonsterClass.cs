﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterClass : Unit
{
    public void Initialize()
    {
        Stats.LVL = 1;

        Stats.VIT = 5 * Stats.LVL;
        Stats.STR = (15 + Stats.VIT / 5) * Stats.LVL;

        Stats.MaxHP = (Stats.VIT * 10) * Stats.LVL;
        Stats.MaxMP = 30 * Stats.LVL;

        EXPval = 25 * Stats.LVL;

        CurrentHP = Stats.MaxHP;
        CurrentMP = Stats.MaxMP;
    }

    //public int CurrentHP()
    //{

    //}
}

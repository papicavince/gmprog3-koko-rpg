﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerExpBar : MonoBehaviour
{
    private Image expBar;

    public GameObject player;
    private HeroineClass playerClass;

    private void Awake()
    {
        expBar = GetComponent<Image>();
    }

    private void Start()
    {
        playerClass = player.GetComponent<PlayerStats>().player;
    }

    private void LateUpdate()
    {
        // Gets reference of the player
        playerClass = player.GetComponent<PlayerStats>().player;

        // Sets the fill amount of the image component equal to the quotient of current xp and max xp
        expBar.fillAmount = (float)playerClass.CurrentEXP / (float)playerClass.Stats.MaxEXP;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    #region Public Variables
    public HeroineClass player;
    private NormalAttack normalAttack;
    private GroundCrush groundCrush;
    public Heal heal;
    public Blessing blessing;

    public bool enableStartingStats;

    public int startingHP;
    public int startingMP;
    public int startingSP;
    #endregion

    #region Private Variables
    private float manaRate = 0.5f;
    private float lastRegen = 0.0f;
    #endregion

    private void Awake()
    {
        // Creates Player Class & Skills
        player = new HeroineClass();
        normalAttack = new NormalAttack();
        groundCrush = new GroundCrush();
        blessing = new Blessing();
        heal = new Heal();
    }

    // Use this for initialization
    void Start()
    {
        // Initializes Player Class & Skills
        player.Initialize();

        // Adds Skills to player
        player.Skill.Add(normalAttack);
        player.Skill.Add(groundCrush);
        player.Skill.Add(blessing);
        player.Skill.Add(heal);

        // Sets starting stats accordingly
        if (enableStartingStats)
        {
            player.CurrentHP = startingHP;
            player.CurrentMP = startingMP;
            player.statPoints = startingSP;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Constantly regens mana
        if (Time.time > manaRate + lastRegen)
        {
            player.CurrentMP += player.Stats.MPRegen;
            lastRegen = Time.time;
        }
    }

    // Activates Ground Crush Skill
    public void GroundCrush()
    {
        player.Skill[1].activateSkill(player, player);
    }

    // Activates Blessing Skill
    public void Blessing()
    {
        player.Skill[2].activateSkill(player, player);
    }

    // Activates Heal Skill
    public void Heal()
    {
        player.Skill[3].activateSkill(player, player);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    #region Public Variables
    public float meleeRange = 3f;
    public float moveSpeed = 10;
    public float rotSpeed = 5;

    public GameObject marker;
    public Vector3 offset = new Vector3(0, 0, 0);
    #endregion

    #region Private Variables
    private Vector3 targetPosition;
    private NavMeshAgent agent;
    private PlayerAnimationController animationController;

    private bool isIdle;
    private bool isMovingToMarker;
    private bool isMovingToEnemy;
    private bool isAttacking;
    private bool isAttackingStandby;
    [HideInInspector] public bool isSkilling;

    private Vector3 lookAtTarget;
    private GameObject enemyObject;
    private GameObject interactableObject;
    private Quaternion playerRot;

    private GameObject point;
    #endregion

    // Use this for initialization
    void Start()
    {
        // Assigns the components from the inspector
        animationController = GetComponent<PlayerAnimationController>();
        agent = this.GetComponent<NavMeshAgent>();

        // Assigns default target position as it's own transform.position
        targetPosition = transform.position;

        // Player is standing still, hence not moving to a marker
        isMovingToMarker = false;

        // Assigns NavMeshAgent's speed
        agent.speed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        // Clicking anywhere that is considered "Ground" moves the player to that location
        ClickToMove();

        // If the player is not moving then they are idle
        if (isIdle)
        {
            // Stops NavMeshAgent when idle
            agent.isStopped = true;

            // Calls Idle animation
            animationController.AnimateIdle();
        }

        // If the player is moving
        else if (isMovingToMarker)
        {
            // Allows the NavMeshAgent to move
            agent.isStopped = false;

            // Calls Normal Running animation
            animationController.AnimateNormalRun();

            // Move to where the player clicked
            MoveToMarker();
        }

        // If the player is moving towards an enemy
        else if (isMovingToEnemy)
        {
            // Allows the NavMeshAgent to move
            agent.isStopped = false;

            // Calls Normal Running animation
            animationController.AnimateAttackRun();

            // Move to where the enemy is
            MoveToAttack();
        }

        // If the player is in range of the enemy
        else if (isAttacking)
        {
            // Stops the NavMeshAgent to attack
            agent.isStopped = true;

            // Calls Attack Animation
            animationController.AnimateAttack();

            // Player rotates towards targeted enemy
            if (enemyObject != null)
            {
                playerRot = Quaternion.LookRotation(enemyObject.transform.position);

                // Smoothens rotation
                transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);
            }
            // Condition when enemy is not in range anymore
            else if (enemyObject == null)
            {
                isAttacking = false;
                isAttackingStandby = true;
            }
        }

        // If the targeted enemy is no longer in range
        else if (isAttackingStandby)
        {
            agent.isStopped = true;
            animationController.AnimateAttackStandby();
        }

        // If the player is playing a skill animation
        if (isSkilling)
        {
            agent.isStopped = true;
            animationController.AnimateSkill();
        }
    }

    #region Animation Events
    void DamageTo(string value)
    {
        MonsterClass enemyMonster = enemyObject.GetComponent<EnemyStats>().monster;
        HeroineClass playerHeroine = this.gameObject.GetComponent<PlayerStats>().player;

        // Activates Skill against targeted monster
        playerHeroine.Skill[0].activateSkill(playerHeroine, enemyMonster);
    }

    void StartEffect(string value)
    {

    }

    void StopEffect(string value)
    {

    }
    #endregion

    void ClickToMove()
    {
        if (Input.GetMouseButtonDown(1))
        {
            // Right clicking means moving, hence you are no longer idle
            isIdle = false;

            // Destroys marker to prevent multiple markers
            Destroy(point);

            // Checks if where player clicked is considered walkable
            SetTargetPosition();
        }
    }

    void SetTargetPosition()
    {
        // Raycast on where player clicked
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Declares hit as where the raycast hit
        RaycastHit hit;

        // Move to Position if where the raycast hit is "Ground"
        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.transform != null)
            {
                // If player clicks the ground
                if (hit.transform.gameObject.tag == "Ground")
                {
                    MoveToPosition(hit);

                    // Disables targeted enemy marker
                    if (enemyObject != null) enemyObject.transform.GetChild(0).gameObject.SetActive(false);
                    else if (interactableObject != null) interactableObject.transform.GetChild(0).gameObject.SetActive(false);
                }

                // If the player clicks on an interactable object
                else if (hit.transform.gameObject.tag == "Interactable")
                {
                    MoveToPosition(hit);

                    // Disables targeted enemy marker
                    if (enemyObject != null) enemyObject.transform.GetChild(0).gameObject.SetActive(false);
                }

                // If the player clicks on an enemy
                else if (hit.transform.gameObject.tag == "Enemy")
                {
                    MoveToEnemy(hit);

                    if (interactableObject != null) interactableObject.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }
    }

    void MoveToPosition(RaycastHit hit)
    {
        // Target Position is where the raycast hit
        targetPosition = hit.point;

        // Character looks at where target position is
        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
                                   transform.position.y,
                                   targetPosition.z - transform.position.z);

        // Assigns player rotation to look at target position                           
        playerRot = Quaternion.LookRotation(lookAtTarget);

        isMovingToMarker = true;

        // Instantiates marker on where the character's destination is
        if (hit.transform.gameObject.tag == "Ground") point = Instantiate(marker, targetPosition + offset, transform.rotation);
    }

    void MoveToMarker()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);

        // Sets NavMeshAgent's agent destination to target position
        agent.SetDestination(targetPosition);

        // Stop's the agent, and destroys the marker when almost arriving at destination
        if (Vector3.Distance(transform.position, targetPosition) <= 0.5f)
        {
            Destroy(point);
            isMovingToMarker = false;
            isIdle = true;
        }
    }

    void MoveToEnemy(RaycastHit hit)
    {
        // Sets target position to where the player clicked
        targetPosition = hit.point;

        // Looks at the target (enemy)
        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
                                transform.position.y,
                                targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);

        // Transitions to move towards enemy
        isMovingToMarker = false;
        isMovingToEnemy = true;

        targetPosition = hit.transform.gameObject.transform.position;
        enemyObject = hit.transform.gameObject;

        // Enables Enemy Marker
        enemyObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    void MoveToAttack()
    {
        // Activates the marker to indicate the enemy has been targeted
        enemyObject.transform.GetChild(0).gameObject.SetActive(true);

        // Smoothens rotation towards target
        playerRot = Quaternion.LookRotation(lookAtTarget);
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, rotSpeed * Time.deltaTime);

        // Sets NavMeshAgent's agent destination to the enemy's position
        agent.SetDestination(enemyObject.transform.position);

        // Transitions player to attack when the player is close enough
        if (Vector3.Distance(enemyObject.transform.position, agent.transform.position) <= meleeRange)
        {
            isMovingToEnemy = false;
            isAttacking = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManaBar : MonoBehaviour
{
    private Image manaBar;

    public GameObject player;
    private HeroineClass playerClass;

    private void Awake()
    {
        manaBar = GetComponent<Image>();
        playerClass = player.GetComponent<PlayerStats>().player;
    }

    private void LateUpdate()
    {
        // Gets reference of the player
        playerClass = player.GetComponent<PlayerStats>().player;

        // Sets the fill amount of the image component equal to the quotient of current mp and max mp
        manaBar.fillAmount = (float)playerClass.CurrentMP / (float)playerClass.Stats.MaxMP;
    }
}

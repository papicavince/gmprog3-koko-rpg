﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Skill
{
    // Sets the Name of the skill and MP Cost
    public Heal()
    {
        Name = "Heal";
        MpCost = 15;
        Cooldown = 5;
        ActiveCooldown = 0;
    }

    public override void activateSkill(Unit actor, Unit target)
    {
        if (actor.CurrentMP < MpCost)
        {
            Debug.Log("Not enough MP to cast " + Name);
            return;
        }

        Debug.Log("Activating" + Name);
        amount = (int)(actor.MaxHP * 0.25f);

        actor.CurrentHP += amount + actor.Stats.MATK;

        actor.CurrentMP -= MpCost;
    }

}

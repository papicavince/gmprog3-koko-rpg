﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCrush : Skill
{
    // Sets the Name of the skill and MP Cost
    public GroundCrush()
    {
        Name = "Ground Crush";
        MpCost = 10;
        Cooldown = 1;
        ActiveCooldown = 0;
        AOEradius = 15f;
    }

    public override void activateSkill(Unit actor, Unit target)
    {
        //GameObject actorObject = actor.PlayerGameObject();

        //animationController = actorObject.GetComponent<PlayerAnimationController>();
        //animationController.AnimateSkill();



        //Debug.Log(actorObject.GetComponent<PlayerStats>().name);


        Debug.Log("Activating " + Name);

        actor.CurrentMP -= MpCost;

        amount = (actor.STR / 2 + (actor.Stats.MATK)) ;

        AOE(actor, amount, AOEradius);

        //AreaAttack();


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillCooldown : MonoBehaviour
{
    public Image imageCooldown;
    private float cooldown = 5;
    private bool isCooldown;

    public GameObject player;
    private HeroineClass unit;

    public int skillID;
    private Skill skill;

    private float time;
    private bool isDeactivated = false;

    private void Start()
    {
        unit = player.GetComponent<PlayerStats>().player;

        StartCoroutine(LateStart(0.01f));
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        skill = unit.Skill[skillID];
        cooldown = skill.Cooldown + skill.ActiveCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCooldown)
        {
            time += Time.deltaTime;

            //imageCooldown.fillAmount = 1;

            //imageCooldown.fillAmount -= cooldown * Time.deltaTime / 1;

            imageCooldown.fillAmount += 1 / cooldown * Time.deltaTime;

            if (imageCooldown.fillAmount >= 1)
            {
                imageCooldown.fillAmount = 0;
                isCooldown = false;

                //skill.isActive = false;

                Debug.Log("Cooldown finished");

                time = 0;
            }

            if (time >= skill.ActiveCooldown)
            {
                //if (skill.isActive) return;
                //else
                //{
                //    skill.deactivateSkill(unit, unit);
                //    //isDeactivated = true;
                //    //skill.isActive = false;
                //}
                if (skill.isActive)
                {
                    skill.deactivateSkill(unit, unit);
                    //time = 0;
                }
            }


        }
    }

    public void ActivateSkill()
    {
        if (isCooldown)
        {
            Debug.Log("Skill is in cooldown");
        }
        else if (unit.CurrentMP <= skill.MpCost)
        {
            Debug.Log("Not enough MP to cast " + skill.Name);
        }
        else
        {
            skill.activateSkill(unit, unit);
            isCooldown = true;
            //isDeactivated = false;
            //skill.isActive = true;
        }
    }

    public void ActivateAnimSkill()
    {
        if (isCooldown)
        {
            Debug.Log("Skill is in cooldown");
        }
        else if (unit.CurrentMP <= skill.MpCost)
        {
            Debug.Log("Not enough MP to cast " + skill.Name);
        }
        else
        {
            player.GetComponent<PlayerMovement>().isSkilling = true;

            //skill.activateSkill(unit, unit);
            isCooldown = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blessing : Skill
{
    // Sets the Name of the skill and MP Cost
    public Blessing()
    {
        Name = "Blessing";
        MpCost = 20;
        Cooldown = 5;
        ActiveCooldown = 5;
        isActive = false;
    }

    public override void activateSkill(Unit actor, Unit target)
    {
        if (actor.CurrentMP <= MpCost)
        {
            Debug.Log("Not enough MP to cast " + Name);
            return;
        }

        Debug.Log("Activating " + Name);

        amount = actor.Stats.MATK;

        actor.Stats.STR += amount;

        actor.CurrentMP -= MpCost;

        isActive = true;
    }

    public override void deactivateSkill(Unit actor, Unit target)
    {
        Debug.Log("Active time finished");

        actor.Stats.STR -= amount;
        isActive = false;
    }
}

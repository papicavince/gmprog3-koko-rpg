﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransform : MonoBehaviour
{
    public Vector3 playerCameraPosition;
    public Quaternion playerCameraRotation;

    public Vector3 monstersCameraPosition;
    public Quaternion monstersCameraRotation;

    public Vector3 npcCameraPosition;
    public Quaternion npcCameraRotation;

    // Switches camera transform position to their respective positions
    public void CameraPlayer()
    {
        Camera.main.transform.position = playerCameraPosition;
        Camera.main.transform.rotation = playerCameraRotation;
    }

    public void CameraMonsters()
    {
        Camera.main.transform.position = monstersCameraPosition;
        Camera.main.transform.rotation = monstersCameraRotation;
    }

    public void CameraNPC()
    {
        Camera.main.transform.position = npcCameraPosition;
        Camera.main.transform.rotation = npcCameraRotation;
    }
}

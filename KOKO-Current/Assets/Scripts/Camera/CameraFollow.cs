﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    void LateUpdate()
    {
        // Desired position is to where the camera's destination is
        Vector3 desiredPosition = target.position + offset;

        // Lerps between the position of the camera's supposed destination, and camera's current position
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        // Sets camera's current position
        transform.position = smoothedPosition;
    }
}

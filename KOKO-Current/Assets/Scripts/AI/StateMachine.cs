﻿namespace StateStuff
{
    public class StateMachine<T>
    {
        // Current State in the StateMachine e.g. Attack, Patrol, Idle, Chase
        public State<T> currentState { get; private set; }
        // Used to reference the AI gameObject
        public T Owner;

        public StateMachine(T _o)
        {
            Owner = _o;
            currentState = null;
        }

        // Changes the current state to a new state via parameter
        public void ChangeState(State<T> _newstate)
        {
            // Exits the current state
            if (currentState != null) currentState.ExitState(ref Owner);

            // Sets current state to the new state via parameter
            currentState = _newstate;

            // New current state plays enter function
            currentState.EnterState(ref Owner);
        }

        public void Update() { if (currentState != null) currentState.UpdateState(ref Owner); }
    }

    public abstract class State<T>
    {
        // Function to be called everytime when entering a new state. Acts as the Start Function of MonoBehaviour
        public abstract void EnterState(ref T _owner);

        // An Update Function that's called in the respective state after Enter state is called. Acts as the Update Function of MonoBehaviour
        public abstract void UpdateState(ref T _owner);

        // Function that is called when changing to a new state
        public abstract void ExitState(ref T _owner);
    }
}
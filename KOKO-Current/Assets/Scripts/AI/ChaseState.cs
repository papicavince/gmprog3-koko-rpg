﻿using UnityEngine;
using StateStuff;

public class ChaseState : State<AI>
{
    private static ChaseState _instance;

    private ChaseState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static ChaseState Instance
    {
        get
        {
            if (_instance == null)
            {
                new ChaseState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isChase = true;

        // Changes current speed to the faster chase speed
        _owner.currentSpeed = _owner.chaseSpeed;

        //Debug.Log("Enter Chase State");
    }

    public override void ExitState(ref AI _owner)
    {
        _owner.isIdle = false;
        _owner.isPatrol = false;
        _owner.isChase = false;
        _owner.isAttack = false;

        // Resets current speed to normal speed just in case
        _owner.currentSpeed = _owner.normalSpeed;

        Debug.Log("Exit Chase State");
    }

    public override void UpdateState(ref AI _owner)
    {
        _owner.agent.SetDestination(_owner.player.position);

        // Extends detection range when chasing compared to Idle State
        if (Vector3.Distance(_owner.player.position, _owner.transform.position) >= _owner.detectionRange + 2.9f)
        {
            Debug.Log("Idle From Chase");

            // Chases player when in detection range
            _owner.stateMachine.ChangeState(IdleState.Instance);
            _owner.switchState = !_owner.switchState;
        }

        else if (Vector3.Distance(_owner.player.position, _owner.transform.position) <= _owner.meleeRange)
        {
            Debug.Log("Attack From Chase");

            // Changes to Attack State when player is in melee range
            _owner.stateMachine.ChangeState(AttackState.Instance);
            _owner.switchState = !_owner.switchState;
        }
    }
}

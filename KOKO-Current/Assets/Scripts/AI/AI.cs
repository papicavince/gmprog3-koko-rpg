﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using StateStuff;

public class AI : MonoBehaviour
{
    public bool switchState = false;

    // Timer used in Update States
    public float gameTimer;
    public int seconds = 0;

    // Radius on how far AI will patrol
    public float patrolRadius = 2.9f;

    [HideInInspector] public StateMachine<AI> stateMachine { get; set; }

    // How far AI will detect player
    public float detectionRange;

    // Range before AI will play Attack Animation
    public float meleeRange;

    // Bools to check current state
    public bool isIdle = false;
    public bool isPatrol = false;
    public bool isChase = false;
    public bool isAttack = false;

    // Gets Player position
    [HideInInspector] public Transform player;

    // Sets NavMeshAgent's speed
    public float currentSpeed;

    // Normal Idle or Patrol State speed
    public float normalSpeed = 4.2f;

    // Chase State speed
    public float chaseSpeed = 6.9f;

    private Animator animator;

    [HideInInspector] public NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        // Finds Player Reference
        player = GameObject.FindGameObjectWithTag("Player").transform;

        // Sets NavMeshAgent's speed to the default normal speed
        currentSpeed = normalSpeed;

        // Creates new state machine and assign it to this
        stateMachine = new StateMachine<AI>(this);

        // Sets AI State to Idle
        stateMachine.ChangeState(IdleState.Instance);

        gameTimer = Time.time;
    }

    private void Update()
    {
        // When certain bool is active it plays to its respective animation
        if (isIdle)
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Run", false);
        }
        else if (isPatrol)
        {
            animator.SetBool("Walk", true);
            animator.SetBool("Run", false);
            animator.SetBool("Idle", false);
        }
        else if (isChase)
        {
            animator.SetBool("Run", true);
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Attack", false);
        }
        else if (isAttack)
        {
            animator.SetBool("Attack", true);
            animator.SetBool("Run", false);
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
        }

        stateMachine.Update();
    }

    // Finds random point in NavMesh. Used in Patrol State
    public Vector3 RandomNavSphere(Vector3 origin, float distance, int layermask)
    {
        // Sets random direction in a sphere scaled on the distance variable
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;

        // Adds random direction to self to get random direction sphere around the AI
        randomDirection += origin;

        NavMeshHit navHit;

        // Finds a random point in the NavMesh
        NavMesh.SamplePosition(randomDirection, out navHit, distance, layermask);

        // Returns random point found in the NavMesh
        return navHit.position;
    }
}
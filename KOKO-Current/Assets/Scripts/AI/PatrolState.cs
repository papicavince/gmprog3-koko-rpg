﻿using UnityEngine;
using StateStuff;

public class PatrolState : State<AI>
{
    private static PatrolState _instance;

    private PatrolState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PatrolState Instance
    {
        get
        {
            if (_instance == null)
            {
                new PatrolState();
            }

            return _instance;
        }
    }

    public override void EnterState(ref AI _owner)
    {
        _owner.isPatrol = true;

        // Sets current speed to normal speed
        _owner.currentSpeed = _owner.normalSpeed;

        // Finds position to patrol to
        Vector3 newPos = _owner.RandomNavSphere(_owner.transform.position, _owner.patrolRadius, -1);

        // Sets NavMeshAgent to the random patrol point
        _owner.agent.SetDestination(newPos);

        Debug.Log("Enter Patroll State");
    }

    public override void ExitState(ref AI _owner)
    {
        _owner.isIdle = false;
        _owner.isPatrol = false;
        _owner.isChase = false;
        _owner.isAttack = false;

        Debug.Log("Exit Patrol State");
    }

    public override void UpdateState(ref AI _owner)
    {
        if (Vector3.Distance(_owner.agent.destination, _owner.transform.position) <= 0.114f)
        {
            // Changes to Idle Stance once AI reached its destination
            _owner.stateMachine.ChangeState(IdleState.Instance);
            _owner.switchState = !_owner.switchState;
        }

        if (Vector3.Distance(_owner.player.position, _owner.transform.position) <= _owner.detectionRange)
        {
            Debug.Log("Chase From Patrol");

            // Chases player if there is a player nearby
            _owner.stateMachine.ChangeState(ChaseState.Instance);
            _owner.switchState = !_owner.switchState;
        }
    }
}

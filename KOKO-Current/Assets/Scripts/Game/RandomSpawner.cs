﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    // List of monsters to spawn
    public GameObject[] monsters;

    // Lists of spawn positions
    public Transform[] spawnPoints;

    // Max monsters to spawn
    public int maxMonsterCount = 10;

    // How much it spawns
    public float spawnRate = 3f;
    private float lastSpawn = 0;

    private int monsterIndex;
    private int spawnIndex;


    // Use this for initialization
    void Start()
    {
        // Gets random spawn position
        spawnIndex = Random.Range(0, spawnPoints.Length);

        // Gets random monster to spawn
        monsterIndex = Random.Range(0, monsters.Length);
    }

    // Update is called once per frame
    void Update()
    {
        // Limits the number of spawns
        if (maxMonsterCount >= 10)
        {
            if (Time.time > spawnRate + lastSpawn)
            {
                // Chooses random monster to spawn from a random spawn position
                GameObject monster = Instantiate(monsters[monsterIndex], spawnPoints[spawnIndex].transform.position, transform.rotation);

                // Gets a new random spawn position
                spawnIndex = Random.Range(0, spawnPoints.Length);
                // Gets a new random monster
                monsterIndex = Random.Range(0, monsters.Length);

                lastSpawn = Time.time;
            }
        }
    }
}

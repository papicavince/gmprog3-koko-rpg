﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveButtons : MonoBehaviour
{
    public GameObject[] buttons;

    // Used as a button On Click Function
    // Clicking another button disables all the buttons from the previous character models
    public void PressPlayerButton()
    {
        // Disables all character models' buttons
        foreach (GameObject button in buttons) button.gameObject.SetActive(false);

        // Only enables the recently pressed one
        buttons[0].gameObject.SetActive(true);
    }

    public void PressMonstersButton()
    {
        foreach (GameObject button in buttons) button.gameObject.SetActive(false);

        buttons[1].gameObject.SetActive(true);
    }

    public void PressNpcButton()
    {
        foreach (GameObject button in buttons) button.gameObject.SetActive(false);

        buttons[2].gameObject.SetActive(true);
    }
}
